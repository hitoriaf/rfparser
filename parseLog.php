<?php
class ParseLog
{
    public $source;
    public $extractResult;
    protected $activeLineValue;
    protected $childCounter = 0;
    protected $nameChild;
    public static function setSource($path)
    {
        $source = file_get_contents($path);
        $parser = new ParseLog;
        $parser->source = $source;
        return $parser;
    }
    private function _serializeCharData($index,$data){
        switch ($index) {
            case 10: case 13:
                break;
            default:
                $arrData = explode(': ',$data);
                break;
        }
        if(isset($arrData)){
            return $arrData;
        }
        return null;
    }
    private function _normalizeDate($value){
        preg_match('/\[(.*)\/(.*) (.*)\]/', $value, $array);
        $data['day'] = $array[2];
        $data['month'] = $array[1];
        $data['time'] = isset($array[3])?$array[3]:"NaN";

        return $data;

    }
    private function _serializePickup($value){
        preg_match('/PICK UP: (.*) mob(.*){(.*)} (.*)/', trim($value), $pickup);
        // $value = str_replace("PICK UP", "EA", $value);
        if(!isset($pickup[1])){
            return [];
        }
        $data['item'] = $pickup[1];
        $data['mob'] = str_replace(")","",str_replace("(","",$pickup[2]));
        $data['map'] = $pickup[3];
        $data['date']  = $this->_normalizeDate($pickup[4]);
        return $data;
    }
    private function _serializeDump($value){
        preg_match('/DUMP: (.*){(.*)} (.*)/', $value, $dump);
        if(!isset($dump[1])){
            return [];
        }
        $data['item'] = $dump[1];
        $data['map'] = $dump[2];
        $data['date'] = $this->_normalizeDate($dump[3]);
        return $data;
    }
    private function _serializeCashItem($value){
        preg_match('/USE CASH ITEM: (.*)  (.*)/', $value, $array);
        if(!isset($array[1])){
            return [];
        }
        $data['item'] = $array[1];
        $data['date'] = $this->_normalizeDate($array[2]);
        return $data;
    }
    private function _isSellOrBuy($value){
        preg_match('/(.*): num:(.*) (rev|pay)\(D:(.*) G:(.*)\) \$D:(.*) \$G:(.*) \[(.*)]/', $value, $array);
        $childedValues = ['SELL','BUY'];
        if(!isset($array[1])){
            return false;
        }
        if(in_array($array[1],$childedValues)){
            $this->childCounter = $array[2];
            return [
                'type' => $array[1],
                'childNum' => $array[2],
                'transaction' => [
                    'D' => $array[4],
                    'G' => $array[5],
                ],
                'total' => [
                    'D' => $array[6],
                    'G' => $array[7],
                ],
                'date' => $this->_normalizeDate("[".$array[8]."]"),
             ];
        }
    }
    function extract(){
        $array = explode("\n", $this->source);
        $result = [];
        $separatorLineCount = 0;
        $categories = ['EQUIP','EMBELL','INVEN','FORCE','RES','ANIMUS'];
        // print_r($array);die;
        foreach ($array as $lineNum => $lineValue) {
            if($lineNum <= 19){
                $lineValueArray = $this->_serializeCharData($lineNum,$lineValue);
                if(!is_null($lineValueArray) && isset($lineValueArray[1])){
                    $result['baseData'][$lineValueArray[0]] = $lineValueArray[1];
                }
            }
            if($lineNum >= 20){
                if(strpos($lineValue,"============")){
                    $separatorLineCount++;
                }
                if($separatorLineCount >= 5 ){
                    $this->logStartNum = $lineNum + 1;
                    break;
                }else{
                    if(in_array(trim($lineValue),$categories)){
                        $this->activeLineValue = trim($lineValue);
                        continue;
                    }
                    $result['items'][$this->activeLineValue][] = $lineValue;
                }
                
            }
        }
        foreach ($array as $lineNum => $lineValue) {
            if($lineNum > $this->logStartNum){
                if(!empty($lineValue)){
                    $pickups = $this->_serializePickup($lineValue);
                    $dumps = $this->_serializeDump($lineValue);
                    $cashItems = $this->_serializeCashItem($lineValue);
                    if(isset($pickups['item'])){
                        $result['pickups'][] = $pickups;
                    }
                    if(isset($dumps['item'])){
                        $result['dumps'][] = $dumps;
                    }
                    if(isset($cashItems['item'])){
                        $result['cashItem'][] = $cashItems;
                    }
                    $isSellOrBuy = $this->_isSellOrBuy($lineValue);
                    if($isSellOrBuy){
                        $this->nameChild = $isSellOrBuy['type'];
                        $result[$isSellOrBuy['type']][] = $isSellOrBuy;
                    }
                    // if($this->childCounter > 0){
                    //     $result[$this->nameChild]
                    // }
                }
            }
        }
        unset($result['items']['ANIMUS']);
        $this->extractResult = $result;
        return $result;
    }
}
